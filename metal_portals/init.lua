for _,metal in ipairs({"steel","copper"}) do
	--
	-- RAY
	-- 
	local register_portal_ray = function(name ,texture, post_effect_color)

		minetest.register_node(name, {
			tiles = {
				"ray_y_tile.png",
				"ray_y_tile.png",
				texture,
				texture,
				texture,
				texture
			},
			paramtype="light",
			use_texture_alpha = true,
			walkable = false,
			pointable = false,
			diggable = false,
			drawtype = "nodebox",
			is_ground_content = false,
			buildable_to = true,
			light_source = LIGHT_MAX - 1,
			post_effect_color = post_effect_color, 
			node_box = element_portals.node_box.cylinder_box_14,
			selection_box = element_portals.node_box.normal_box
		})
		
		minetest.register_abm({
			nodenames = {name} ,
			interval = 5,
			chance = 1,
			action = function(pos, node, active_object_count, active_object_count_wider)
				local result = element_portals:node_on_axis(pos, "portal_active", -4, -1, "y")
				if result.count == 0 then 	
					minetest.set_node(pos, {name="air"})
				end			 
			end
		})
		
		
	end

	register_portal_ray("metal_portals:steel_portal_ray_top", "steel_portal_ray_top.png", { r=125, g=198, b=198, a=50 })


	--
	-- PORTALS
	-- 

	local steel_portal_params =  {
		 -- Fuel requiered to active the portal once
		 fuel_stack = "default:"..metal.."_ingot",
		 -- Fuel to place around the portal to keep it activate
		 fuel_surrounding = "default:"..metal.."block",
		 fuel_surrounding_count = 4,
		 -- Type of portal IN_PORTAL, OUT_PORTAL, IN_OUT_PORTAL, IN_RANDOM
		 portal_type = element_portals.IN_OUT_PORTAL,
		 -- Group of this portal
		  portal_groups = {metal.."_portals"},
		 -- Group of portals assosciated to this one
		 -- (Can teleport to/from this one)
		filter_group = metal.."_portals",
		 -- Node of the active portal
		 active_node = "metal_portals:"..metal.."_portal_active", 
		 -- Node of the inactive portal
		 inactive_node = "metal_portals:"..metal.."_portal",
		 -- Active swaping beetwen nodes when portal is active / inactive
		 swap_enabled = true,
		 -- Replacement of nodes around
		 replace_surroundings = {
			except_nodes_containing_in_name = {metal},
			offsets = {
			-- if unspecified replacement_node_names, air is the default 
				{pos = {x=-1,z=1}},
				{pos = {x= 1,z=-1}},
				{pos = {x= 1,z=1}},
				{pos = {x=-1,z=-1}},
				{pos = {y=-1}},
				{pos = {y = 1}, replacement_node_name="metal_portals:steel_portal_ray_top"},
				{pos = {y = 2}},
			}
		 }
		}

	local bloc_texture = "default_glass.png"
		
	element_portals:register_public_portal_node("metal_portals:"..metal.."_portal_active", {
		description = "Active "..metal.." Portal",
		tiles = {
			{name="portal_"..metal.."_anim_active.png", animation={type="vertical_frames", aspect_w=16, aspect_h=16, length=2.0}}
		},

		is_ground_content = true,
		drawtype = "nodebox",
		node_box = element_portals.node_box.normal_box,
		selection_box = element_portals.node_box.normal_box,
		paramtype="light",
		drop = "metal_portals:"..metal.."_portal",
		groups = {cracky=3, not_in_creative_inventory = 1}
	}, steel_portal_params)

	element_portals:register_public_portal_node("metal_portals:"..metal.."_portal", {
		description = metal.." Portal",
		tiles = {
			{name="portal_"..metal.."_anim.png", animation={type="vertical_frames", aspect_w=16, aspect_h=16, length=2.0}}
		},
		is_ground_content = true,
		drawtype = "nodebox",
		node_box =element_portals.node_box.normal_box,
		selection_box = element_portals.node_box.normal_box,
		paramtype="light",
		groups = {cracky=3}
	}, steel_portal_params)

	element_portals:register_portal_abm(steel_portal_params)
	
	local ingot = 'default:'..metal..'_ingot'
	minetest.register_craft({
		output = "metal_portals:"..metal.."_portal",
		recipe = {
			{ingot, ingot, ingot},
			{ingot, 'default:mese_crystal_fragment', ingot},
			{ingot, ingot, ingot}
		}
	})


end

