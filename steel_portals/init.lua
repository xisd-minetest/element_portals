

--
-- RAY
-- 
local register_portal_ray = function(name ,texture, post_effect_color)

	minetest.register_node(name, {
		tiles = {
			"ray_y_tile.png",
			"ray_y_tile.png",
			texture,
			texture,
			texture,
			texture
		},
		paramtype="light",
		use_texture_alpha = true,
		walkable = false,
		pointable = false,
		diggable = false,
		drawtype = "nodebox",
		is_ground_content = false,
		buildable_to = true,
		light_source = LIGHT_MAX - 1,
		post_effect_color = post_effect_color, 
		node_box = element_portals.node_box.cylinder_box_14,
		selection_box = element_portals.node_box.normal_box
	})
	
	minetest.register_abm({
		nodenames = {name} ,
		interval = 5,
		chance = 1,
		action = function(pos, node, active_object_count, active_object_count_wider)
			local result = element_portals:node_on_axis(pos, "portal_active", -4, -1, "y")
			if result.count == 0 then 	
				minetest.set_node(pos, {name="air"})
			end			 
		end
	})
	
	
end

register_portal_ray("steel_portals:steel_portal_ray_bottom", "steel_portal_ray_bottom.png", { r=125, g=198, b=198, a=50 })
register_portal_ray("steel_portals:steel_portal_ray_top", "steel_portal_ray_top.png", { r=125, g=198, b=198, a=50 })


--
-- PORTALS
-- 

local steel_portal_params =  {
	 -- Fuel requiered to active the portal once
	 fuel_stack = "default:steel_ingot 2",
	 -- Fuel to place around the portal to keep it activate
	 fuel_surrounding = "default:steelblock",
	 fuel_surrounding_count = 4,
	 -- Type of portal IN_PORTAL, OUT_PORTAL, IN_OUT_PORTAL, IN_RANDOM
	 portal_type = element_portals.IN_OUT_PORTAL,
	 -- Group of this portal
	  portal_groups = {"steel_portals"},
	 -- Group of portals assosciated to this one
	 -- (Can teleport to/from this one)
	filter_group = "steel_portals",
	 -- Node of the active portal
	 active_node = "steel_portals:steel_portal_active", 
	 -- Node of the inactive portal
	 inactive_node = "steel_portals:steel_portal",
	 -- Active swaping beetwen nodes when portal is active / inactive
	 swap_enabled = true,
	 -- Replacement of nodes around
	 replace_surroundings = {
		except_nodes_containing_in_name = {"steel"},
		offsets = {
		-- if unspecified replacement_node_names, air is the default 
			{pos = {x=-1,z=1}},
			{pos = {x= 1,z=-1}},
			{pos = {x= 1,z=1}},
			{pos = {x=-1,z=-1}},
			{pos = {y=-1}},
			{pos = {y = 1}, replacement_node_name="steel_portals:steel_portal_ray_bottom"},
			{pos = {y = 2}, replacement_node_name="steel_portals:steel_portal_ray_top"}
		}
	 }
	}

local bloc_texture = "default_glass.png"
	
element_portals:register_public_portal_node("steel_portals:steel_portal_active", {
	description = "Active Steel Portal",
	tiles = {
		{name="portal_steel_anim_active.png", animation={type="vertical_frames", aspect_w=16, aspect_h=16, length=2.0}}
	},

	is_ground_content = true,
	drawtype = "nodebox",
	node_box = element_portals.node_box.normal_box,
	selection_box = element_portals.node_box.normal_box,
	paramtype="light",
	groups = {cracky=3, not_in_creative_inventory = 1}
}, steel_portal_params)

element_portals:register_public_portal_node("steel_portals:steel_portal", {
	description = "Steel Portal",
	tiles = {
		{name="portal_steel_anim.png", animation={type="vertical_frames", aspect_w=16, aspect_h=16, length=2.0}}
	},
	is_ground_content = true,
	drawtype = "nodebox",
	node_box =element_portals.node_box.normal_box,
	selection_box = element_portals.node_box.normal_box,
	paramtype="light",
	groups = {cracky=3}
}, steel_portal_params)

element_portals:register_portal_abm(steel_portal_params)

minetest.register_craft({
	output = "steel_portals:steel_portal 2",
	recipe = {
		{'default:steelblock', 'default:steel_ingot', 'default:steelblock'},
		{'default:steel_ingot', 'default:mese_crystal_fragment', 'default:steel_ingot'},
		{'default:steelblock', 'default:steel_ingot', 'default:steelblock'}
	}
})
