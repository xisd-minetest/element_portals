-- Kept for compatiblility 
-- Will be removed soon
-- 


local mese_portal_params=  {
 -- Fuel requiered to active the portal once
 fuel_stack = "default:mese_crystal_fragment 1",
 -- Fuel to place around the portal to keep it activate
 fuel_surrounding = "default:mese",
 fuel_surrounding_count = 4,
 -- Type of portal IN_PORTAL, OUT_PORTAL, IN_OUT_PORTAL, IN_RANDOM
 portal_type = element_portals.IN_RANDOM_PORTAL,
 -- Group of this portal
  portal_groups = {"mese_portals"},
 -- Group of portals assosciated to this one
 -- (Can teleport to/from this one)
 --filter_group = "mese_portals",
 -- Node of the active portal
 active_node = "mese_portals:mese_portal_active", 
 -- Node of the inactive portal
 inactive_node = "mese_portals:mese_portal",
 -- Active swaping beetwen nodes when portal is active / inactive
 swap_enabled = true,
 -- Replacement of nodes around
 replace_surroundings = {
 	except_nodes_containing_in_name = {"water_source", "lava_source", "lava_flowing", "water_flowing", "sand_portal_vortex", "mese" },
 	offsets = {
 	-- if unspecified replacement_node_names, air is the default 
 		{pos = {x=-1}},
 		{pos = {x= 1}},
 		{pos = {z = -1}},
 		{pos = {z=1}},
 		{pos = {y= -1}},
 		{pos = {y = 1}, replacement_node_name="sand_portals:sand_portal_vortex"},
 		{pos = {y = 2}, replacement_node_name="sand_portals:sand_portal_vortex"}
 	}
 }
}

element_portals:register_public_portal_node("mese_portals:mese_portal_active", {
	description = "Mese Portal",
	tiles = {{name="sand_portal_anim.png", animation={type="vertical_frames", aspect_w=16, aspect_h=16, length=1.0}}, "default_mese_block.png^default_glass.png", "default_mese_block.png^default_glass.png", "default_mese_block.png^default_glass.png","default_mese_block.png^default_glass.png", "default_mese_block.png^default_glass.png"},
	is_ground_content = true,
	drawtype = "nodebox",
	node_box = element_portals.node_box.box_with_carved_cone,
	selection_box = element_portals.node_box.normal_box,
	paramtype="light",
	groups = {cracky=3, not_in_creative_inventory = 1}
}, mese_portal_params)

element_portals:register_public_portal_node("mese_portals:mese_portal", {
	description = "Mese Portal",
	tiles = {"default_mese_block.png^portal_glass.png",  "default_mese_block.png^default_glass.png", "default_mese_block.png^default_glass.png", "default_mese_block.png^default_glass.png","default_mese_block.png^default_glass.png", "default_mese_block.png^default_glass.png"},

	is_ground_content = true,
	drawtype = "nodebox",
	node_box = element_portals.node_box.box_with_carved_cone,
	selection_box = element_portals.node_box.normal_box,
	paramtype="light",
	groups = {cracky=3}
}, mese_portal_params)

element_portals:register_portal_abm(mese_portal_params)

-- minetest.register_craft({
	-- output = 'mese_portals:mese_portal',
	-- recipe = {
		-- {'default:glass', 'default:mese_crystal', 'default:glass'},
		-- {'default:glass', 'default:mese_crystal', 'default:glass'},
		-- {'default:glass', 'default:glass', 'default:glass'}
	-- }
-- })
