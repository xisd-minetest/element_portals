Element Portals (Forked form mod by curieuxx)
===============

>A [Minetest](http://minetest.net/) mod that adds teleport capabilities in the game trough the natural elements found in the world. Portals can have a custom name and you can select your destination. The mod is separated in submodules so you can chose to use a subset of these portals.

Changes from this fork were initally quickfixes, somes are not quite polished and most still need testing and/or optimizing

----------------------
### Changes from this fork ###
----------------------

- API changes
	- [x] fix public portals api : Anyone can use public portal network from placer (even if placer is not online)
	- [x] Channel system (See description below)
			
* __Metal portal__ [new] (bronze an steel) Can be used by other players, use ingots as fuel
 	- [x] In-Out public portal (see API changes)
	- [x] Animated textures

- Tree portal
	- [x] Only grows roots on group soil (nodes where a tree can be build)
	- [x] Roots needs to be placed near group soil

- [x] Use groups for liquids, wood and saplings
- [x] Teleport_free command require server privilege
- [x] active portals, vortex and fields are not displayed in creative inventory

And some minor bugfixes (undeclared global, etc...)

old New things :
----------------

* __Mese portal__ : Still exist but crafting recipe is commented out
    _It was interesting as an experiment but not so usefull in the end, so will not be updated anymore and is likely to be removed soon_
    Teleports you to a random place in the world, use mese crystal fragment as fuel 
	- [x] can be used by anyone (public, see API changes)
	- [x] teleport player to a random location
	- [x] carve a landing zone if surface was not found


ToDo :
----

- [ ] Tree portal root only link to where it was dug from
- [x] Tree portal root need to be placed ON group soil
- [ ] dirt portal and stone box as intended in original mod

- [ ] Support for intllib  
- [ ] Translation  
- [ ] Support for help modpack 
- [ ] Support for awards  

- Maybe public portal should also link to player portals as well as placer's




Channel system description
----------------------------

Portals can be named  **<channel>:<portal_name>**

				or	  **<channel>-<other_portal_name>**
				or	  **<channel>** (only channel as portal name)

Those portals will link to each other as long as they have the same channel
Portals without "**:**" will also link to every other portals without channel
And portals without channel will link to every other portals without "**:**" in the name


exemple : 
		portal 1 is named mymine:crosspath
		portal 2 is named mountain
		portal 3 is named mymine-blablabla
		portal 4 is named mymine
		portal 5 is named myminestairway
		portal 6 is named mymine:waterpool
		portal 7 is named mygarden:waterpool
						
	Portal 1 will link only to every portal of channel *mymine*, 
		meaning those starting with *mymine:* or *mymine-* and those named *mymine* only
			so portal 1 link to 3,4,6
	Portal 2 has not channel, it will link only to every portal not starting with '<channel>:' 
			so portal 2 link to 3,4,5
	portal 3 and 4 are both in the network of portal without channel and to the *mymine* network
			so portal  3 link to 1,2,4,5,6
			and portal 4 link to 1,2,3,5,6
	portal 5 has no channel, exactly like portal 2
			so portal 5 link to 2,3,4
	Portal 6 will link only to every portal of channel *mymine*, exactly like portal 1 
			so portal 6 link to 1,3,4
	Portal 7 will link only to every portal of channel *mygarden*,
		meaning those starting with *mygarden:* or *mygarden-* and those named *mygarden* only
			so portal 7 link to nothing


----------------------
### Description from original mod by curieuxx ###
----------------------

A [Minetest](http://minetest.net/) mod that adds teleport capabilities in the game trough the natural elements found in the world. Portals can have a custom name and you can select your destination. The mod is separated in submodules so you can chose to use a subset of these portals.


Screnshots
----------

![Water Portal](https://gist.githubusercontent.com/curieuxx/0e51be3854dfb0be7358/raw/water_portal.png)

![Lava portal](https://gist.githubusercontent.com/curieuxx/0e51be3854dfb0be7358/raw/lava_portal.png)

![Tree portal](https://gist.githubusercontent.com/curieuxx/0e51be3854dfb0be7358/raw/tree_portal.png)

![Tree portal root](https://gist.githubusercontent.com/curieuxx/0e51be3854dfb0be7358/raw/tree_portal_root.png)

![Sand portal](https://gist.githubusercontent.com/curieuxx/0e51be3854dfb0be7358/raw/sand_portal.png)

![Desert sand portal](https://gist.githubusercontent.com/curieuxx/0e51be3854dfb0be7358/raw/desert_sand_portal.png)


Usage / Wiki
------------

A wiki with pictures and details is here :  [https://github.com/curieuxx/mod-element_portals/wiki](https://github.com/curieuxx/mod-element_portals/wiki)

Dependencies
------------
Just mods found in defaults:

 - default
 - bucket
 - dye [new]


Install
-------
Download the latest stable pre-release [zip](https://github.com/curieuxx/mod-element_portals/archive/Sanpshot_1.0.0-beta-2014-09-09.zip) / [tarball](https://github.com/curieuxx/mod-element_portals/archive/Sanpshot_1.0.0-beta-2014-09-09.tar.gz) or the master (unstable but with new features) [zip](https://github.com/curieuxx/mod-element_portals/archive/master.zip) /  [tarball](https://github.com/curieuxx/mod-element_portals/archive/master.tar.gz) and unpack into your mintest `mods` as described here: [Installing Mods](http://wiki.minetest.com/wiki/Installing_Mods) or here : [Installing Mods](http://dev.minetest.net/Installing_Mods). Don't forget to enable it from world configuration.


Current portals
---------------

 * __Water portal__ uses water as connection and power, they are private and you can select your end point
 * __Lava portal__ act like water portals but use lava as connection and power
 * __Tree portal__ as home portal, the portal grows roots that can be harvested, when you need to get back home just plant the root and a link will be created to all of your tree portals where you can teleport. The planted root will slowly grow into a tree sapling after a while
 * __Sand portal__ used as fast escapes - just step on it and your on the other end


What is new in the last milestone  ?
-------------


New Portals and Nodes:

 - [Tree portals](https://github.com/curieuxx/mod-element_portals/wiki/Tree-Portals) [Implemented]
 - [Sand portals](https://github.com/curieuxx/mod-element_portals/wiki/Sand-Portals) [Implemented]
 - [Quick sand](https://github.com/curieuxx/mod-element_portals/wiki/Sand-Portals#quick-sand) needed for sand portals [Implemneted] 

 
New technical features :

 - Item portals functions [Implemented] - allows to implement tools or items that act as IN portals
 - Portal filtering by their direction types (IN_OUT, IN, OUT) and groups [Implemented]
 - Allow portals to be powered with surounding elements [implemented]
 - Other utility methods that will help for the future portal nodes [Implemented]
 - '/teleport_free' command [Implemented]
 - Sanitize portals - Checks for data and node pair, if they are not in sync then they are fixed - allows introducing new featres without needing to clear world alteration made by previous implementation [implemented]


Bufixes for existing portals : 

 - Portal name is generated with duplicates #9
 - Portal data is not syncronized with the node when burned or pulverized by tnt #10
 - Portal list from portal form contained and empty item #11
 - Overhead in register_abm for liquid portals #12
 - Double consumption on teleport #13
 - Various performance, complexity, DRY and cycle dependency fixes [ongoing]




Under development
-------------------------

   * Dirt portal - Public portal that helps you teleport to nearby players by "digging" tunnels.

   * Stone box - Doesn't teleport a player but works more or less like a locked chest but it's inventory is shared across all stone boxes.

   * Maybe a Mese portal - Expensive to craft but with all the abilities of the other portals elements. "Maybe" mainly because it may shade the other portal types.



Licence
-------

Copyright 2014 Tiberiu Corbu

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

Licence for textures
--------------------

Author 2014 Tiberiu Corbu

Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)
http://creativecommons.org/licenses/by-sa/3.0/
 
Refer to each `README.md` file from the submodules `textures` folder for licence, details about derivations from the original and original authors of individual files used.

These are :

 * [Liquid Portals Textures README.md](./liquid_portals/textures/README.md)
 * [Tree Portals Textures README.md](./tree_portals/textures/README.md)
 * [Sand Portals Textures README.md](./sand_portals/textures/README.md)

Licence for sounds
------------------

Refer to each `README.md` file from the submodules `sounds` folder for licence, details about derivations from the original and original authors of individual files used.
 
These are :

 * [Liquid Portals Sounds README.md](./liquid_portals/sounds/README.md)
 * [Tree Portals Sound README.md](./tree_portals/sounds/README.md)
 


